import 'dart:io';
void main(){
  print("Apakah anda ingin menginstall aplikasi dart?");
  stdout.write("(y/n): ");
  String jawab = stdin.readLineSync()!;

  // menggunakan operator ternary sebagai ganti if/esle
  var hasil = (jawab == 'y') ? "Anda akan menginstall aplikasi dart" : "Aborted";

  print("$hasil");
} 