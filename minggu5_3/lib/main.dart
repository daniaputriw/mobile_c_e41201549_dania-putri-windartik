import 'package:flutter/material.dart';
import 'package:minggu5_3/routes.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}
